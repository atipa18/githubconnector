import SwiftUI

@main
struct GithubConnectorApp: App {
    var body: some Scene {
        WindowGroup {
            RepositoriesView(viewModel: RepositoriesViewModel())
        }
    }
}
