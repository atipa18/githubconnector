struct Commit {
  let authorName: String
  let message: String

  enum CodingKeys: String, CodingKey {
    case authorName = "name"
    case message
    case commit
    case author
  }
}
