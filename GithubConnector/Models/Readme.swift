import Foundation

struct Readme {
    let content: String
    
    enum CodingKeys: String, CodingKey {
        case content
    }
}

// MARK: - Decodable
extension Readme: Decodable, Hashable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        content = try container.decode(String.self, forKey: .content)
    }
}
