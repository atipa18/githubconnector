import SwiftUI

struct RepositoryView: View {
    
    @State var repository = Repository(name: "", fullName: "", description: "", htmlUrl: "", forks: 0, watchers: 0)
    
    @ObservedObject var viewModel: RepositoryViewModel
    
    var body: some View {
        ScrollView{
            Text("Name: " + repository.name)
                .padding()
            
            Text("Full name: " + repository.fullName)
                .padding()

            Text("Description: " + (repository.description ?? ""))
                .padding()

            Text("Url: " + repository.htmlUrl)
                .padding()

            Text("Forks: " + String(repository.forks))
                .padding()

            Text("Watchers: " + String(repository.watchers))
                .padding()
        
            Text("README \n" + viewModel.readmeContent).onAppear{
            viewModel.loadReadmeContent(fullName: repository.fullName)
        }
        }
    }
}
