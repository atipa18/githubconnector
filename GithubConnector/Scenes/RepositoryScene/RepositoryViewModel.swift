import Foundation

final class RepositoryViewModel: ObservableObject, GitAPIManagerInjectable {
    
    @Published private(set) var readmeContent: String = ""
    
    func loadReadmeContent(fullName: String) {
        gitAPIManager.getReadmeFrom(fullname: fullName) { [weak self] readme in
            self?.readmeContent = readme
        }
    }
}
