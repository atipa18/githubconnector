import Foundation

final class RepositoriesViewModel: ObservableObject, GitAPIManagerInjectable {
        
    @Published private(set) var repositories: [Repository] = []
    
    func loadRepositories(query: String) {
        gitAPIManager.searchRepositories(query: query) { [weak self] repositories in
            self?.repositories = repositories
        }
    }
}
