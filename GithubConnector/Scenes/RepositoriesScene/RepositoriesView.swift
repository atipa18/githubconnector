import SwiftUI

struct RepositoriesView: View {
    
    @State var query: String = "iOS"
    
    @ObservedObject var viewModel: RepositoriesViewModel
    
    var body: some View {
        NavigationView{
            VStack{
                TextField(
                    "Search github for",
                    text: $query,
                    onCommit: {
                        viewModel.loadRepositories(query: query)
                    }
                )
                    .navigationBarTitle("Search on github", displayMode: .inline)
                    .background(.white)
                    .foregroundColor(.gray)
                    .padding()
                    .onAppear {
                        viewModel.loadRepositories(query: query)
                    }
                
                List(viewModel.repositories, id: \.name) { repository in
                    NavigationLink(repository.name, destination: RepositoryView(repository: repository, viewModel: RepositoryViewModel()))
                }
            }
        }
    }
}
